﻿//x,y,rotateは初期座標、sizeは0から1の範囲で
var Obj = function (Id, size, x, y, rotate) {
    var Parts = function () {
        this.x = 0;
        this.y = 0;
        this.number = 0;
    }
    this.nT600;
    this.nT450;
    this.nT300;
    this.nT250;
    this.nT200;
    this.nT150;
    this.nT80;
    this.nRR450;
    this.nLR450;
    this.nRR600;
    this.nLR600;
    this.nRS38;
    this.nLS38;
    this.nRLC;
    this.nLLC;
    this.nUp;
    this.nDown;
    this.number = 0;
    this.display = function (str) {
        switch (str) {
            case "T600":
                this.nT600.number++;
                break;
            case "T450":
                this.nT450.number++;
                break;
            case "T300":
                this.nT300.number++;
                break;
            case "T250":
                this.nT250.number++;
                break;
            case "T200":
                this.nT200.number++;
                break;
            case "T150":
                this.nT150.number++;
                break;
            case "T80":
                this.nT80.number++;
                break;
            case "RR450":
                this.nRR450.number++;
                break;
            case "LR450":
                this.nLR450.number++;
                break;
            case "RR600":
                this.nRR600.number++;
                break;
            case "LR600":
                this.nLR600.number++;
                break;
            case "RS38":
                this.nRS38.number++;
                break;
            case "LS38":
                this.nLS38.number++;
                break;
            case "RLC":
                this.nRLC.number++;
                break;
            case "LLC":
                this.nLLC.number++;
                break;
            case "Up":
                this.nUp.number++;
                break;
            case "Down":
                this.nDown.number++;
                break;
            default:
                break;
        }
        this.number++;

        this.x = this.defaultx + this.nT600.x + this.nT450.x + this.nT300.x +
        this.nT250.x + this.nT200.x + this.nT150.x + this.nT80.x + this.nRR450.x + this.nLR450.x +
        this.nRR600.x + this.nLR600.x + this.nRS38.x + this.nLS38.x + this.nRLC.x + this.nLLC.x + this.nUp.x + this.nDown.x;
        this.y = this.defaulty + this.nT600.y + this.nT450.y + this.nT300.y +
        this.nT250.y + this.nT200.y + this.nT150.y + this.nT80.y + this.nRR450.y + this.nLR450.y +
        this.nRR600.y + this.nLR600.y + this.nRS38.y + this.nLS38.y + this.nRLC.y + this.nLLC.y + this.nUp.y + this.nDown.y;

        document.getElementById("nT600").innerHTML = this.nT600.number;
        document.getElementById("nT450").innerHTML = this.nT450.number;
        document.getElementById("nT300").innerHTML = this.nT300.number;
        document.getElementById("nT250").innerHTML = this.nT250.number;
        document.getElementById("nT150").innerHTML = this.nT150.number;
        document.getElementById("nR450").innerHTML = this.nRR450.number + this.nLR450.number;
        document.getElementById("nR600").innerHTML = this.nRR600.number + this.nLR600.number;
        document.getElementById("nS38").innerHTML = this.nRS38.number + this.nLS38.number;
        document.getElementById("nRLC").innerHTML = this.nRLC.number;
        document.getElementById("nLLC").innerHTML = this.nLLC.number;
        document.getElementById("nUp").innerHTML = this.nUp.number;
        document.getElementById("nDown").innerHTML = this.nDown.number;

        document.getElementById("x").innerHTML = obj.x;
        document.getElementById("y").innerHTML = obj.y;
        document.getElementById("rotate").innerHTML = obj.rotate;

        if (this.number == 1) {
            location.hash += str;
        } else {
            location.hash += "," + str;
        }
    }
    //ああ
    this.double = size;
    this.x = this.defaultx = x;
    this.y = this.defaulty = y;
    this.rotate = this.defaultrotate = rotate;
    var canvas = document.getElementById(Id);
    canvas.width = 10000;
    canvas.height = 10000;
    var ctx = canvas.getContext("2d");
    var imageData;
    var data;
    //センサの値を取得
    this.dis = function (x, y) {
        imageData = ctx.getImageData(x, y, 3, 3);
        //ctx.putImageData(imageData, 0, 0);
        data = imageData.data;
        var r = 0, g = 0, b = 0;
        for (var i = 0; i < 9; i++) {
            r += data[i * 4];
            g += data[i * 4 + 1];
            b += data[i * 4 + 2];
        }
        return r / 9 * 0.299 + g / 9 * 0.587 + b / 9 * 0.114;
    }
    //初期化
    this.init = function (x, y, rotate) {
        this.nT600 = new Parts();
        this.nT450 = new Parts();
        this.nT300 = new Parts();
        this.nT250 = new Parts();
        this.nT200 = new Parts();
        this.nT150 = new Parts();
        this.nT80 = new Parts();
        this.nRR450 = new Parts();
        this.nLR450 = new Parts();
        this.nRR600 = new Parts();
        this.nLR600 = new Parts();
        this.nRS38 = new Parts();
        this.nLS38 = new Parts();
        this.nRLC = new Parts();
        this.nLLC = new Parts();
        this.nUp = new Parts();
        this.nDown = new Parts();
        this.number = 0;
        this.double = size;
        this.x = this.defaultx = x;
        this.y = this.defaulty = y;
        this.rotate = this.defaultrotate = rotate;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        document.getElementById("nT600").innerHTML = 0;
        document.getElementById("nT450").innerHTML = 0;
        document.getElementById("nT300").innerHTML = 0;
        document.getElementById("nT250").innerHTML = 0;
        document.getElementById("nT150").innerHTML = 0;
        document.getElementById("nR450").innerHTML = 0;
        document.getElementById("nR600").innerHTML = 0;
        document.getElementById("nS38").innerHTML = 0;
        document.getElementById("nRLC").innerHTML = 0;
        document.getElementById("nLLC").innerHTML = 0;
        document.getElementById("nUp").innerHTML = 0;
        document.getElementById("nDown").innerHTML = 0;

        document.getElementById("x").innerHTML = 0;
        document.getElementById("y").innerHTML = 0;
        document.getElementById("rotate").innerHTML = 0;
        location.hash = this.x + "," + this.y + "," + this.rotate + "&";
    }
    //直線600mm
    this.T600 = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 600);
        ctx.lineTo(300, 600);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 600);
        ctx.lineTo(270, 600);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 600);
        ctx.lineTo(170, 600);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 600);
        ctx.lineTo(160, 600);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nT600.x += -Math.sin(this.rotate * Math.PI / 180) * 600;
        this.nT600.y += Math.cos(this.rotate * Math.PI / 180) * 600;
        this.display("T600");
    }
    //直線450mm
    this.T450 = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 450);
        ctx.lineTo(300, 450);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 450);
        ctx.lineTo(270, 450);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 450);
        ctx.lineTo(170, 450);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 450);
        ctx.lineTo(160, 450);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nT450.x += -Math.sin(this.rotate * Math.PI / 180) * 450;
        this.nT450.y += Math.cos(this.rotate * Math.PI / 180) * 450;
        this.display("T450");
    }
    //直線300mm
    this.T300 = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 300);
        ctx.lineTo(300, 300);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 300);
        ctx.lineTo(270, 300);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 300);
        ctx.lineTo(170, 300);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 300);
        ctx.lineTo(160, 300);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nT300.x += -Math.sin(this.rotate * Math.PI / 180) * 300;
        this.nT300.y += Math.cos(this.rotate * Math.PI / 180) * 300;
        this.display("T300");
    }
    //直線250mm
    this.T250 = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 250);
        ctx.lineTo(300, 250);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 250);
        ctx.lineTo(270, 250);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 250);
        ctx.lineTo(170, 250);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 250);
        ctx.lineTo(160, 250);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nT250.x += -Math.sin(this.rotate * Math.PI / 180) * 250;
        this.nT250.y += Math.cos(this.rotate * Math.PI / 180) * 250;
        this.display("T250");
    }
    //直線200mm
    this.T200 = function () {
        var length = 200;
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, length);
        ctx.lineTo(300, length);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, length);
        ctx.lineTo(270, length);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, length);
        ctx.lineTo(170, length);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, length);
        ctx.lineTo(160, length);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nUp.x += -Math.sin(this.rotate * Math.PI / 180) * length;
        this.nUp.y += Math.cos(this.rotate * Math.PI / 180) * length;
        this.display("T200");
    }
    //直線150mm
    this.T150 = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 150);
        ctx.lineTo(300, 150);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 150);
        ctx.lineTo(270, 150);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 150);
        ctx.lineTo(170, 150);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 150);
        ctx.lineTo(160, 150);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nT150.x += -Math.sin(this.rotate * Math.PI / 180) * 150;
        this.nT150.y += Math.cos(this.rotate * Math.PI / 180) * 150;
        this.display("T150");
    }
    //直線80mm
    this.T80 = function () {

        var length = 80;
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, length);
        ctx.lineTo(300, length);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, length);
        ctx.lineTo(270, length);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, length);
        ctx.lineTo(170, length);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, length);
        ctx.lineTo(160, length);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nUp.x += -Math.sin(this.rotate * Math.PI / 180) * length;
        this.nUp.y += Math.cos(this.rotate * Math.PI / 180) * length;
        this.display("T80");
    }
    //カーブ半径600mm
    this.R600 = function (reversal) {
        if (!reversal) {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-750, 0);
            ctx.arc(0, 0, 600, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 900, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-750, 0);
            ctx.arc(0, 0, 630, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 870, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-750, 0);
            ctx.arc(0, 0, 730, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 770, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-750, 0);
            ctx.arc(0, 0, 740, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 760, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            this.nRR600.x += (-Math.cos(this.rotate * Math.PI / 180) + Math.cos((this.rotate + 45) * Math.PI / 180)) * 750;
            this.nRR600.y += (-Math.sin(this.rotate * Math.PI / 180) + Math.sin((this.rotate + 45) * Math.PI / 180)) * 750;
            this.rotate += 45;
            this.display("RR600");
        }
        else {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(750, 0);
            ctx.arc(0, 0, 600, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 900, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(750, 0);
            ctx.arc(0, 0, 630, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 870, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(750, 0);
            ctx.arc(0, 0, 730, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 770, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(750, 0);
            ctx.arc(0, 0, 740, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 760, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();

            this.nLR600.x += (+Math.cos(this.rotate * Math.PI / 180) - Math.cos((-this.rotate + 45) * Math.PI / 180)) * 750;
            this.nLR600.y += (Math.sin(this.rotate * Math.PI / 180) + Math.sin((-this.rotate + 45) * Math.PI / 180)) * 750;
            this.rotate -= 45;
            this.display("LR600");
        }
    }
    //カーブ半径450mm
    this.R450 = function (reversal) {
        if (!reversal) {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-600, 0);
            ctx.arc(0, 0, 450, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 750, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-600, 0);
            ctx.arc(0, 0, 480, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 720, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-600, 0);
            ctx.arc(0, 0, 580, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 620, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-600, 0);
            ctx.arc(0, 0, 590, 0, Math.PI / 180 * 45, false);
            ctx.arc(0, 0, 610, Math.PI / 180 * 45, 0, true);
            ctx.fill();

            ctx.restore();
            this.nRR450.x += (-Math.cos(this.rotate * Math.PI / 180) + Math.cos((this.rotate + 45) * Math.PI / 180)) * 600;
            this.nRR450.y += (-Math.sin(this.rotate * Math.PI / 180) + Math.sin((this.rotate + 45) * Math.PI / 180)) * 600;
            this.rotate += 45;
            this.display("RR450");
        }
        else {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(600, 0);
            ctx.arc(0, 0, 450, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 750, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(600, 0);
            ctx.arc(0, 0, 480, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 720, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(600, 0);
            ctx.arc(0, 0, 580, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 620, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(600, 0);
            ctx.arc(0, 0, 590, Math.PI / 180 * 135, Math.PI / 180 * 180, false);
            ctx.arc(0, 0, 610, Math.PI / 180 * 180, Math.PI / 180 * 135, true);
            ctx.fill();

            ctx.restore();

            this.nLR450.x += (+Math.cos(this.rotate * Math.PI / 180) - Math.cos((-this.rotate + 45) * Math.PI / 180)) * 600;
            this.nLR450.y += (Math.sin(this.rotate * Math.PI / 180) + Math.sin((-this.rotate + 45) * Math.PI / 180)) * 600;
            this.rotate -= 45;
            this.display("LR450");
        }
    }
    //直角
    this.S38 = function (reversal) {
        if (!reversal) {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(0, 0);
            ctx.lineTo(300, 0);
            ctx.lineTo(300, 600);
            ctx.lineTo(-300, 600);
            ctx.lineTo(-300, 300);
            ctx.lineTo(0, 300);
            ctx.lineTo(0, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(30, 0);
            ctx.lineTo(270, 0);
            ctx.lineTo(270, 570);
            ctx.lineTo(-300, 570);
            ctx.lineTo(-300, 330);
            ctx.lineTo(30, 330);
            ctx.lineTo(30, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(130, 0);
            ctx.lineTo(170, 0);
            ctx.lineTo(170, 470);
            ctx.lineTo(-300, 470);
            ctx.lineTo(-300, 430);
            ctx.lineTo(130, 430);
            ctx.lineTo(130, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(140, 0);
            ctx.lineTo(160, 0);
            ctx.lineTo(160, 460);
            ctx.lineTo(-300, 460);
            ctx.lineTo(-300, 440);
            ctx.lineTo(140, 440);
            ctx.lineTo(140, 0);
            ctx.fill();

            ctx.restore();
            //クロスライン
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(0, -40);
            ctx.lineTo(0, -60);
            ctx.lineTo(300, -60);
            ctx.lineTo(300, -40);
            ctx.lineTo(0, -40);
            ctx.fill();

            ctx.restore();

            this.nRS38.x += (-Math.sin(this.rotate * Math.PI / 180) - Math.cos(this.rotate * Math.PI / 180)) * 450;
            this.nRS38.y += (Math.cos(this.rotate * Math.PI / 180) - Math.sin(this.rotate * Math.PI / 180)) * 450;
            this.rotate += 90;
            this.display("RS38");
        }
        else {
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(0, 0);
            ctx.lineTo(0, 600);
            ctx.lineTo(600, 600);
            ctx.lineTo(600, 300);
            ctx.lineTo(300, 300);
            ctx.lineTo(300, 0);
            ctx.lineTo(0, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(30, 0);
            ctx.lineTo(30, 570);
            ctx.lineTo(600, 570);
            ctx.lineTo(600, 330);
            ctx.lineTo(270, 330);
            ctx.lineTo(270, 0);
            ctx.lineTo(30, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(127,127,127)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(130, 0);
            ctx.lineTo(130, 470);
            ctx.lineTo(600, 470);
            ctx.lineTo(600, 430);
            ctx.lineTo(170, 430);
            ctx.lineTo(170, 0);
            ctx.lineTo(130, 0);
            ctx.fill();

            ctx.restore();
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(140, 0);
            ctx.lineTo(140, 460);
            ctx.lineTo(600, 460);
            ctx.lineTo(600, 440);
            ctx.lineTo(160, 440);
            ctx.lineTo(160, 0);
            ctx.lineTo(140, 0);
            ctx.fill();

            ctx.restore();
            //クロスライン
            ctx.save();

            ctx.beginPath();
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.scale(this.double, this.double);
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotate * Math.PI / 180);
            ctx.translate(-150, 0);
            ctx.lineTo(0, -40);
            ctx.lineTo(0, -60);
            ctx.lineTo(300, -60);
            ctx.lineTo(300, -40);
            ctx.lineTo(0, -40);
            ctx.fill();

            ctx.restore();

            this.nLS38.x += (-Math.sin(this.rotate * Math.PI / 180) + Math.cos(this.rotate * Math.PI / 180)) * 450;
            this.nLS38.y += (Math.cos(this.rotate * Math.PI / 180) + Math.sin(this.rotate * Math.PI / 180)) * 450;
            this.rotate -= 90;
            this.display("LS38");
        }
    }
    //右レーンチェンジ
    this.RLC = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 50);
        ctx.lineTo(-300, 50);
        ctx.lineTo(-300, 700);
        ctx.lineTo(0, 700);
        ctx.lineTo(0, 650);
        ctx.lineTo(300, 650);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 50);
        ctx.lineTo(-270, 50);
        ctx.lineTo(-270, 700);
        ctx.lineTo(-30, 700);
        ctx.lineTo(-30, 650);
        ctx.lineTo(270, 650);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 250);
        ctx.lineTo(170, 250);
        ctx.lineTo(170, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 250);
        ctx.lineTo(160, 250);
        ctx.lineTo(160, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(-130, 250);
        ctx.lineTo(-130, 700);
        ctx.lineTo(-170, 700);
        ctx.lineTo(-170, 250);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(-140, 250);
        ctx.lineTo(-140, 700);
        ctx.lineTo(-160, 700);
        ctx.lineTo(-160, 250);
        ctx.fill();

        ctx.restore();
        //右ハーフライン
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, -240);
        ctx.lineTo(150, -240);
        ctx.lineTo(150, -260);
        ctx.lineTo(0, -260);
        ctx.fill();

        ctx.restore();

        this.nRLC.x += -Math.cos(this.rotate * Math.PI / 180) * 300 - Math.sin(this.rotate * Math.PI / 180) * 700;
        this.nRLC.y += -Math.sin(this.rotate * Math.PI / 180) * 300 + Math.cos(this.rotate * Math.PI / 180) * 700;
        this.display("RLC");
    }
    //左レーンチェンジ
    this.LLC = function () {
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, 650);
        ctx.lineTo(300, 650);
        ctx.lineTo(300, 700);
        ctx.lineTo(600, 700);
        ctx.lineTo(600, 50);
        ctx.lineTo(300, 50);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, 650);
        ctx.lineTo(330, 650);
        ctx.lineTo(330, 700);
        ctx.lineTo(570, 700);
        ctx.lineTo(570, 50);
        ctx.lineTo(270, 50);
        ctx.lineTo(270, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, 250);
        ctx.lineTo(170, 250);
        ctx.lineTo(170, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, 250);
        ctx.lineTo(160, 250);
        ctx.lineTo(160, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(430, 250);
        ctx.lineTo(430, 700);
        ctx.lineTo(470, 700);
        ctx.lineTo(470, 250);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(440, 250);
        ctx.lineTo(440, 700);
        ctx.lineTo(460, 700);
        ctx.lineTo(460, 250);
        ctx.fill();

        ctx.restore();
        //右ハーフライン
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(150, -240);
        ctx.lineTo(300, -240);
        ctx.lineTo(300, -260);
        ctx.lineTo(150, -260);
        ctx.fill();

        ctx.restore();

        this.nLLC.x += Math.cos(this.rotate * Math.PI / 180) * 300 - Math.sin(this.rotate * Math.PI / 180) * 700;
        this.nLLC.y += Math.sin(this.rotate * Math.PI / 180) * 300 + Math.cos(this.rotate * Math.PI / 180) * 700;
        this.display("LLC");
    }
    //上り坂
    this.Up = function () {
        var length = 120 * Math.sqrt(154) + 300;
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,0,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, length);
        ctx.lineTo(300, length);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, length);
        ctx.lineTo(270, length);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, length);
        ctx.lineTo(170, length);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, length);
        ctx.lineTo(160, length);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nUp.x += -Math.sin(this.rotate * Math.PI / 180) * length;
        this.nUp.y += Math.cos(this.rotate * Math.PI / 180) * length;
        this.display("Up");
    }
    //下り坂
    this.Down = function () {
        var length = 120 * Math.sqrt(154) + 300;
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(0, 0);
        ctx.lineTo(0, length);
        ctx.lineTo(300, length);
        ctx.lineTo(300, 0);
        ctx.lineTo(0, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(30, 0);
        ctx.lineTo(30, length);
        ctx.lineTo(270, length);
        ctx.lineTo(270, 0);
        ctx.lineTo(30, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(127,127,127)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(130, 0);
        ctx.lineTo(130, length);
        ctx.lineTo(170, length);
        ctx.lineTo(170, 0);
        ctx.lineTo(130, 0);
        ctx.fill();

        ctx.restore();
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = "rgb(255,255,255)";
        ctx.scale(this.double, this.double);
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotate * Math.PI / 180);
        ctx.translate(-150, 0);
        ctx.lineTo(140, 0);
        ctx.lineTo(140, length);
        ctx.lineTo(160, length);
        ctx.lineTo(160, 0);
        ctx.lineTo(140, 0);
        ctx.fill();

        ctx.restore();
        this.nDown.x += -Math.sin(this.rotate * Math.PI / 180) * length;
        this.nDown.y += Math.cos(this.rotate * Math.PI / 180) * length;
        this.display("Down");
    }
}
//シミュレーター
var Simulator = function () {
    var Timer, pattern = 0;
    //マイコンカーの座標
    var x = 0, y = 0;
    //センサのデジタル値
    var sv, SP;
    //今走ってるコース
    var NowCourse = 0;
    //0:サーボの曲がり角[rad]
    //1:曲がる前の機体の傾き[rad]
    //2:あれ[rad]
    //3:サーボの現在の角度（１度ずつしか動かない）[deg]
    //4:曲がる前の機体の傾き[deg]
    var deg = new Array(0, 0, 0, 0, 0);

    //方向キーでマイコンカーを動かすやつ
    //シミュレータでセンサを動かすときはOFFにしないといけない
    var Key_control = setInterval(function () {
        micon_car.sensor_move();
    }, 0);
    //cubeをマイコンカーのように動かしてみよう!!
    this.start = function () {
        //キー制御をオフにし、シミュレーターを動かす。
        clearInterval(Key_control);
        pattern = 0;
        //コースのデータを取得
        CD.courseData_input();
        Timer = setInterval(function () {
            //センサーの値をデジタル化
            sv = sensor.check();
            //センサの中心距離を何とか
            document.getElementById("sensordis_output").innerHTML = SP = sensor.getSensorPos();
            //メイン関数
            main();
            //マイコンカーを中心に画面を移動
            deg[4] = Math.round(deg[1] * 180 / Math.PI);
            display_move();
            if (KeyState[116]) {
                //F5が押されたら
                x = Math.round(x);
                y = Math.round(y);
                deg[3] = 0;
                motor.servo(0);
                Key_control = setInterval(function () {
                    micon_car.sensor_move();
                }, 0);
                clearInterval(Timer);
                return false;
            }
        }, 0);
    }
    //方向キーでマイコンカーを動かしてくれるやつ
    this.sensor_move = function (num) {
        if (KeyState[13]) {
            //ライントレース開始
            this.start();
            document.getElementById("side").style.visibility = "hidden";
            canvas_bar_graph.style.visibility = "hidden";
            canvas_line_graph.style.visibility = "hidden";
        }
        deg[1] = deg[4] * Math.PI / 180;
        if (KeyState[37] || num == 1) {
            //左
            x -= Math.cos(deg[1]);
            y -= Math.sin(deg[1]);
        }
        if (KeyState[38] || num == 3) {
            //↑
            x += Math.sin(deg[1]);
            y -= Math.cos(deg[1]);
        }
        if (KeyState[39] || num == 2) {
            //右
            x += Math.cos(deg[1]);
            y += Math.sin(deg[1]);
        }
        if (KeyState[40] || num == 4) {
            //↓
            x -= Math.sin(deg[1]);
            y += Math.cos(deg[1]);
        }

        if (KeyState[88]) {
            //X
            deg[4] += 1;
        }
        if (KeyState[90]) {
            //Z
            deg[4] -= 1;
        }
        if (num != undefined || KeyState[37] || KeyState[38] || KeyState[39] || KeyState[40] || KeyState[88] || KeyState[90]) {
            sensor.move();
            document.getElementById("sensordis_output").innerHTML = sensor.getSensorPos();
            sensor.display();
            for (var i = 0; i < 6; i++) {
                bar_graph.clearRect(i * 100 + 25, 0, 50, canvas_bar_graph.height - sensor.value[i])
                bar_graph.save();

                bar_graph.beginPath();
                bar_graph.fillStyle = "rgb(0,0,0)";
                bar_graph.fillRect(i * 100 + 25, canvas_bar_graph.height - sensor.value[i], 50, sensor.value[i]);

                bar_graph.restore();
            }
            display_move();
            graph.graph_draw(1);
        }
    }

    var cube = document.getElementById("cube").style;
    cube.transformOrigin = "50% 100%";
    document.getElementById("sensorboard").style.transformOrigin = "50% 280px";

    //ライントレースのメイン関数
    function main() {
        switch (pattern) {
            case 0:
                //通常トレース
                if (SP > 0 || sensor.d[5]) {
                    motor.servo(-89);
                }
                else if (SP < 0 || sensor.d[0]) {
                    motor.servo(89);
                }
                motor.accel(8);
                if (sensor.d[6] && sensor.d[7]) {
                    //クロスライン検出
                    pattern = 10;
                }
                if (sensor.d[6] && !sensor.d[7]) {
                    //右ハーフライン検出
                    pattern = 20;
                }
                if (!sensor.d[6] && sensor.d[7]) {
                    //左ハーフライン検出
                    pattern = 30;
                }
                break;
            case 10:
                motor.accel(4);
                if (!sensor.d[0] && !sensor.d[5] && !sensor.d[6] && !sensor.d[7]) {
                    pattern = 11;
                }
                break;
            case 11:
                //クロスライン検出後のライントレース
                if (sensor.d[6]) {
                    //右曲り
                    pattern = 12;
                }
                if (sensor.d[7]) {
                    //左曲り
                    pattern = 14;
                }
                if (SP > 0 || sensor.d[5]) {
                    motor.servo(-89);
                }
                else if (SP < 0 || sensor.d[0]) {
                    motor.servo(89);
                }
                motor.accel(8);
                break;
            case 12:
                //右
                if (!sensor.d[0] && !sensor.d[6]) {
                    pattern = 13;
                }
                motor.accel(1);
                break;
            case 13:
                if (sensor.d[3]) {
                    pattern = 40;
                }
                motor.servo(89);
                motor.accel(1);
                break;
            case 14:
                //左
                if (!sensor.d[5] && !sensor.d[7]) {
                    pattern = 15;
                }
                motor.accel(1);
                break;
            case 15:
                if (sensor.d[2]) {
                    pattern = 40;
                }
                motor.servo(-89);
                motor.accel(1);
                break;
            case 20:
                motor.accel(4);
                if (sensor.d[7]) {
                    //右ハーフラインで左のセンサが反応→クロスライン
                    pattern = 10;
                }
                else if (!sensor.d[0] && !sensor.d[6]) {
                    pattern = 21;
                }
                break;
            case 21:
                //右ハーフライン検出後のライントレース
                if (sv == 0) {
                    pattern = 22;
                }
                if (SP > 0 || sensor.d[5]) {
                    motor.servo(-89);
                }
                else if (SP < 0 || sensor.d[0]) {
                    motor.servo(89);
                }
                motor.accel(8);
                break;
            case 22:
                if (sensor.d[3]) {
                    pattern = 40;
                }
                motor.servo(20);
                motor.accel(4);
                break;
            case 30:
                motor.accel(4);
                if (sensor.d[0]) {
                    //左ハーフラインで右のセンサが反応→クロスライン
                    pattern = 10;
                }
                else if (!sensor.d[5] && !sensor.d[7]) {
                    pattern = 31;
                }
                break;
            case 31:
                //左ハーフライン検出後のライントレース
                if (sv == 0) {
                    pattern = 32;
                }
                if (SP > 0 || sensor.d[5]) {
                    motor.servo(-89);
                }
                else if (SP < 0 || sensor.d[0]) {
                    motor.servo(89);
                }
                motor.accel(8);
                break;
            case 32:
                if (sensor.d[2]) {
                    pattern = 40;
                }
                motor.servo(-20);
                motor.accel(4);
                break;
            case 40:
                //安定するまで通常ライントレースには戻らない
                if (!sensor.d[1] && !sensor.d[4] && ((!sensor.d[2] && sensor.d[3]) || (sensor.d[2] && !sensor.d[3]))) {
                    //センサの値が安定したら通常ライントレースに戻る
                    pattern = 0;
                }
                if (SP > 0 || sensor.d[5]) {
                    motor.servo(-89);
                }
                else if (SP < 0 || sensor.d[0]) {
                    motor.servo(89);
                }
                motor.accel(4);
                break;
        }
    }
    //モーターを動かすやつ
    var Motor = function () {
        var l;
        var accele_lf, accele_rf, accele_lr, accele_rr;
        var r1, r2, r3, r4, r5;
        this.servo = function (servo) {
            //少しづつ曲がる様子を再現
            if (servo > deg[3]) {
                deg[3]++;
            }
            else if (servo < deg[3]) {
                deg[3]--;
            }
            document.getElementById("sensorboard").style.transform = "rotate(" + deg[3] + "deg)";
        }
        this.accel = function (speed) {
            //speed=11で5pixel
            deg[0] = deg[3] * Math.PI / 180;
            if (deg[3] == 0) {
                x += Math.sin(deg[1]) * speed;
                y -= Math.cos(deg[1]) * speed;
            }
            else if (Math.abs(deg[3]) == 90) {
                //めんどくさいので後で
            }
            else if (deg[3] > 0) {
                l = wheelbase / Math.tan(deg[0]);
                deg[2] = speed / Math.sqrt(Math.pow(wheelbase, 2) + Math.pow(l + (tread / 2.000), 2));
                x += l * (Math.cos(deg[1]) - Math.cos(deg[1] + deg[2]));
                y += l * (Math.sin(deg[1]) - Math.sin(deg[1] + deg[2]));
                deg[1] = deg[1] + deg[2];
            }
            else if (deg[3] < 0) {
                l = wheelbase / Math.tan(deg[0]);
                deg[2] = speed / -Math.sqrt(Math.pow(wheelbase, 2) + Math.pow(l - (tread / 2.000), 2));
                x += l * (Math.cos(deg[1]) - Math.cos(deg[1] + deg[2]));
                y += l * (Math.sin(deg[1]) - Math.sin(deg[1] + deg[2]));
                deg[1] = deg[1] + deg[2];
            }
            document.getElementById("cube").style.top = y + "px";
            document.getElementById("cube").style.left = x + "px";
            document.getElementById("cube").style.transform = "rotate(" + deg[1] + "rad)";
            /*******************/
            /*if (deg[3] != 0) {
                //中心
                r1 = wheelbase / Math.tan(Math.abs(deg[0]));
                //lf(rf)
                r2 = Math.sqrt(Math.pow(wheelbase, 2) + Math.pow(r1 + (tread / 2.000), 2));
                //rf(lf)
                r3 = Math.sqrt(Math.pow(wheelbase, 2) + Math.pow(r1 - (tread / 2.000), 2));
                //lr(rr)
                r4 = r1 + (tread / 2.000);
                //rr(lr)
                r5 = r1 - (tread / 2.000);
                if (deg[3] > 0) {
                    //サーボ角度が＋のとき
                    accele_lf = speed * 100;
                    accele_rf = speed * 100 * r3 / r2;
                    accele_lr = speed * 100 * r4 / r2;
                    accele_rr = speed * 100 * r5 / r2;
                }
                else if (deg[3] < 0) {
                    //サーボ角度が－のとき
                    accele_lf = speed * 100 * r3 / r2;
                    accele_rf = speed * 100;
                    accele_lr = speed * 100 * r5 / r2;
                    accele_rr = speed * 100 * r4 / r2;
                }
            }
            else if (deg[3] == 0) {
                //サーボ角度が0のとき
                accele_lf = speed * 100;
                accele_rf = speed * 100;
                accele_lr = speed * 100;
                accele_rr = speed * 100;
            }
            document.getElementById("lf_speed").innerHTML = Math.round(accele_lf);
            document.getElementById("rf_speed").innerHTML = Math.round(accele_rf);
            document.getElementById("lr_speed").innerHTML = Math.round(accele_lr);
            document.getElementById("rr_speed").innerHTML = Math.round(accele_rr);
            *//*******************/
        }
    }
    //センサについてのやつ
    //右端から0～5。6：右デジタル、7：左デジタル
    var Sensor = function () {
        //センサのアナログ値
        this.value = new Array();
        //センサのデジタル値
        this.d = new Array();
        //折れ線グラフのx軸
        this.cnt = 0;
        //センサとセンサバーの中心の距離。
        this.dis = new Array(-45, -27, -9, 9, 27, 45);
        //センサの中心距離を求めたり
        this.getSensorPos = function () {
            //中心からの距離を求めよう
            var max = 0, maxlen;
            for (var i = 0; i < 6; i++) {
                if (max <= this.value[i]) {
                    max = this.value[i];
                    maxlen = i;
                }
            }
            if (this.value[maxlen + 1] == 0) {
                return this.dis[maxlen] - (12 * this.value[maxlen - 1]) / (this.value[maxlen - 1] + this.value[maxlen]) - 3;
            }
            else if (this.value[maxlen - 1] == 0) {
                return this.dis[maxlen] + (12 * this.value[maxlen + 1]) / (this.value[maxlen + 1] + this.value[maxlen]) + 3;
            }
            else {
                return this.dis[maxlen] + ((6 * this.value[maxlen + 1]) / (this.value[maxlen - 1] + this.value[maxlen + 1]) - 3);
            }
        }
        //センサの値を表示したり
        this.display = function () {
            //8個のセンサの値を取得しよう
            for (var i = 0; i < 8; i++) {
                this.value[i] = obj.dis(document.getElementById("main").scrollLeft + document.getElementById("s" + i).getBoundingClientRect().left, document.getElementById("main").scrollTop + document.getElementById("s" + i).getBoundingClientRect().top);
                document.getElementById("ns" + i).innerHTML = Math.round(this.value[i]);
            }
        }
        //マイコンカーを動かすやつ
        this.move = function () {
            document.getElementById("cube").style.left = x + "px";
            document.getElementById("cube").style.top = y + "px";
            document.getElementById("cube").style.transform = "rotate(" + deg[4] + "deg)";
        }
        this.check = function () {
            var res = 0;
            this.display();
            for (var i = 0; i < 8; i++) {
                if (this.value[i] > 160) {
                    //センサ値が160より大きい→白色
                    this.d[i] = true;
                    res += Math.pow(2, i);
                }
                else {
                    //それ以外→灰色・黒
                    this.d[i] = false;
                }
            }
            return res & 0x3f;
        }
    }
    var courseData = function () {
        //データの種類
        var course_kind = function () {
            //0:未定義
            //notDefined
            //1:直線
            //Straight
            //2:上り
            //Up
            //3:下り
            //Down
            //4:急な左カーブ
            //L450
            //5:急な右カーブ
            //R450
            //6:緩やかな左カーブ
            //L600
            //7:緩やかな右カーブ
            //R600
            //8:左レーンチェンジ
            //LLC
            //9:右レーンチェンジ
            //RLC
            //10:左クランク
            //LCR
            //11:右クランク
            //RCR
            this.kind = 0;
            //パルス数
            this.Pulse = 0;
            //1パルス当たりの長さmm
            const MM = 21 * Math.PI / 72;
            //入力された長さ(mm)をパルス数に変換しない
            this.convert_unit = function (distance) {
                this.Pulse += distance;// / MM;
            }
            //それに突入したときの長さ
            this.Position = 0;
        }
        this.Data = new Array();
        //location.hashからコースのデータを取得し、いい感じにthis.Dataに突っ込む
        this.courseData_input = function () {
            //URLのハッシュ以降を取得
            var Data = location.hash.replace("#", "").split("&");
            Data = Data[1].split(",");
            var i = 0, j = -1;
            this.Data[j] = new course_kind();
            while (Data[i] != undefined) {
                switch (Data[i]) {
                    case "T600":
                        if (this.Data[j].kind == 1) {
                            //同じ直線だったら
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(600);
                        }
                        else {
                            //違う直線だったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(600);
                        }
                        break;
                    case "T450":
                        if (this.Data[j].kind == 1) {
                            //同じ直線だったら
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(450);
                        }
                        else {
                            //違う直線だったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(450);
                        }
                        break;
                    case "T300":
                        if (this.Data[j].kind == 1) {
                            //同じ直線だったら
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(300);
                        }
                        else {
                            //違う直線だったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(300);
                        }
                        break;
                    case "T250":
                        if (this.Data[j].kind == 1) {
                            //同じ直線だったら
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(250);
                        }
                        else {
                            //違う直線だったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(250);
                        }
                        break;
                    case "T150":
                        if (this.Data[j].kind == 1) {
                            //同じ直線だったら
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(150);
                        }
                        else {
                            //違う直線だったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 1;
                            this.Data[j].convert_unit(150);
                        }
                        break;
                    case "LR450":
                        //L450
                        if (this.Data[j].kind == 4) {
                            //同じやつだったら
                            this.Data[j].kind = 4;
                            this.Data[j].convert_unit(450 * Math.PI / 2);
                        }
                        else {
                            //違うやつだったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 4;
                            this.Data[j].convert_unit(450 * Math.PI / 2);
                        }
                        break;
                    case "RR450":
                        //R450
                        if (this.Data[j].kind == 5) {
                            //同じやつだったら
                            this.Data[j].kind = 5;
                            this.Data[j].convert_unit(450 * Math.PI / 2);
                        }
                        else {
                            //違うやつだったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 5;
                            this.Data[j].convert_unit(450 * Math.PI / 2);
                        }
                        break;
                    case "LR600":
                        //L600
                        if (this.Data[j].kind == 6) {
                            //同じやつだったら
                            this.Data[j].kind = 6;
                            this.Data[j].convert_unit(600 * Math.PI / 2);
                        }
                        else {
                            //違うやつだったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 6;
                            this.Data[j].convert_unit(600 * Math.PI / 2);
                        }
                        break;
                    case "RR600":
                        //R600
                        if (this.Data[j].kind == 7) {
                            //同じやつだったら
                            this.Data[j].kind = 7;
                            this.Data[j].convert_unit(600 * Math.PI / 2);
                        }
                        else {
                            //違うやつだったら新しいやつにやる
                            j++;
                            this.Data[j] = new course_kind();
                            this.Data[j].kind = 7;
                            this.Data[j].convert_unit(600 * Math.PI / 2);
                        }
                        break;
                    case "LLC":
                        //LLC(長さはハーフラインから線が離れる前までの距離)
                        j++;
                        //直前は直線だと思う
                        this.Data[j - 1].convert_unit(250);
                        this.Data[j] = new course_kind();
                        this.Data[j].kind = 8;
                        this.Data[j].convert_unit(500);
                        break;
                    case "RLC":
                        //RLC(長さはハーフラインから線が離れる前までの距離)
                        j++;
                        //直前は直線だと思う
                        this.Data[j - 1].convert_unit(250);
                        this.Data[j] = new course_kind();
                        this.Data[j].kind = 9;
                        this.Data[j].convert_unit(500);
                        break;
                    case "LS38":
                        //LCR(長さはハーフラインから線が離れる前までの距離)
                        j++;
                        //直前は直線だと思う
                        this.Data[j - 1].convert_unit(450);
                        this.Data[j] = new course_kind();
                        this.Data[j].kind = 10;
                        this.Data[j].convert_unit(500);
                        break;
                    case "RS38":
                        //RCR(長さはハーフラインから線が離れる前までの距離)
                        j++;
                        //直前は直線だと思う
                        this.Data[j - 1].convert_unit(450);
                        this.Data[j] = new course_kind();
                        this.Data[j].kind = 11;
                        this.Data[j].convert_unit(500);
                        break;
                }
                i++;
            }
            Data;
        }
    }
    //グラフを描くためのもの
    var Graph = function () {
        var cnt = 0;
        var sensor_value = new Array();
        for (var i = 0; i < 6; i++) {
            sensor_value[i] = new Array();
        }

        this.graph_draw = function (num) {
            if (num == 1) {
                cnt++;
                for (var i = 0; i < 6; i++) {
                    sensor_value[i][cnt] = sensor.value[i];
                }
            }
            else if (num == 0) {
                //初期化
                cnt = 0;
                line_graph.clearRect(0, 0, canvas_line_graph.width, canvas_line_graph.height);
                for (var i = 0; i < 6; i++) {
                    sensor_value[i] = new Array();
                    sensor_value[i][cnt] = sensor.value[i];
                }
            }
            else if (num == 2) {
                //グラフの作成
                for (var i = 0; i < 6; i++) {
                    line_graph.save();

                    line_graph.beginPath();
                    line_graph.lineWidth = 1;
                    for (var j = 0; j < 600; j++) {
                        line_graph.lineTo(j, sensor_value[i][j]);
                    }
                    line_graph.stroke();

                    line_graph.restore();
                }
                if (canvas_line_graph.style.visibility == "hidden") {
                    //折れ線グラフが表示されていない場合は表示
                    this.graph_change("line");
                }
            }
        }

        this.graph_change = function (graph_type) {
            if (graph_type == "bar") {
                if (canvas_bar_graph.style.visibility == "visible") {
                    document.getElementById("side").style.visibility = "hidden";
                    canvas_bar_graph.style.visibility = "hidden";
                }
                else {
                    document.getElementById("side").style.visibility = "visible";
                    canvas_bar_graph.style.visibility = "visible";
                }
                canvas_line_graph.style.visibility = "hidden";
            }
            else if (graph_type == "line") {
                if (canvas_line_graph.style.visibility == "visible") {
                    document.getElementById("side").style.visibility = "hidden";
                    canvas_line_graph.style.visibility = "hidden";
                }
                else {
                    document.getElementById("side").style.visibility = "visible";
                    canvas_line_graph.style.visibility = "visible";
                }
                canvas_bar_graph.style.visibility = "hidden";
            }
        }
    }
    function display_move() {
        //マイコンカーを中心に画面を移動
        document.getElementById("main").scrollTop = y + wheelbase * (1 - Math.cos(deg[4] * Math.PI / 180)) - (document.getElementById("main").clientHeight / 2);
        document.getElementById("main").scrollLeft = x + tread / 2 + Math.sin(deg[4] * Math.PI / 180) * wheelbase - (document.getElementById("main").clientWidth / 2);
    }
    /***************************************************************/
    var motor = new Motor();
    var CD = new courseData();
    var sensor = new Sensor();
    var graph = new Graph();
    this.graph = graph;
    /**************************************************************/
    this.getData = function () {
        CD.courseData_input();
        document.getElementById("free_window").innerHTML = "";
        document.getElementById("free_window").style.visibility = "visible";
        for (var i = 0; i < CD.Data.length; i++) {
            document.getElementById("free_window").innerHTML += CD.Data[i].kind + ",";
            document.getElementById("free_window").innerHTML += CD.Data[i].Pulse + "<br>";
        }
    }
}

var micon_car;
var obj;
var canvas, ctx;
var Object;
//マイコンカーのwheelbaseとtread
var wheelbase = 200, tread = 159;
function course_create() {
    obj.init(600, 2300, 180);
    obj.T600();
    obj.R450(true);
    obj.R450(false);
    obj.R450(false);
    obj.R450(false);
    obj.R450(false);
    obj.R450(false);
    obj.R450(false);
    obj.R450(true);
    obj.R450(true);
    obj.R450(true);
    obj.R450(true);
    obj.R450(true);
    obj.R450(true);
    obj.R450(false);
    obj.R450(false);
    obj.R450(false);
    obj.T600();
    obj.S38(false);
    obj.T600();
    obj.T600();
    obj.T600();
    obj.T600();
    obj.R450(false);
    obj.R450(false);
    obj.T600();
    obj.T600();
    obj.T600();
    obj.T450();
    obj.S38(false);
    obj.T250();
    obj.RLC();
}
function course_clear() {
    obj.init(500, 0, 0);
}

var canvas_bar_graph, bar_graph;
var canvas_line_graph, line_graph;
window.onload = page_load;
function page_load() {
    window.scroll(0, 0);
    canvas_bar_graph = document.getElementById("bar_graph");
    canvas_bar_graph.style.position = "absolute";
    canvas_bar_graph.width = 600;
    canvas_bar_graph.height = 300;
    canvas_bar_graph.style.backgroundColor = "rgba(255,127,255,0.5)";
    bar_graph = canvas_bar_graph.getContext("2d");
    canvas_line_graph = document.getElementById("line_graph");
    canvas_line_graph.style.position = "absolute";
    canvas_line_graph.width = 600;
    canvas_line_graph.height = 300;
    canvas_line_graph.style.backgroundColor = "rgba(127,255,127,0.5)";
    line_graph = canvas_line_graph.getContext("2d");
    obj = new Obj("red_box", 1, 500, 0, 0);
    micon_car = new Simulator("cube", "sensorboard");
    window_change(0);
    read_hash();
    micon_car.graph.graph_change("bar");
}
function read_hash() {
    if (location.hash.replace("#", "") == "") {
        location.hash = obj.x + "," + obj.y + "," + obj.rotate + "&";
        obj.init(obj.x, obj.y, obj.rotate);
        return false;
    }
    var courseData = location.hash.replace("#", "").split("&");
    if (courseData[0] != 0) {
        courseData[0] = courseData[0].split(",");
        obj.init(parseInt(courseData[0][0]), parseInt(courseData[0][1]), parseInt(courseData[0][2]));
    }
    courseData[1] = courseData[1].split(",");
    var i = 0;
    while (courseData[1][i] != undefined && courseData[1][i] != "" && i != 100) {
        obj_place(courseData[1][i]);
        i++;
    }
}

function obj_place(f) {
    switch (f) {
        case "T80": obj.T80(); break;
        case "T150": obj.T150(); break;
        case "T200": obj.T200(); break;
        case "T250": obj.T250(); break;
        case "T300": obj.T300(); break;
        case "T450": obj.T450(); break;
        case "T600": obj.T600(); break;
        case "RR450": obj.R450(false); break;
        case "LR450": obj.R450(true); break;
        case "RR600": obj.R600(false); break;
        case "LR600": obj.R600(true); break;
        case "RS38": obj.S38(false); break;
        case "LS38": obj.S38(true); break;
        case "RLC": obj.RLC(); break;
        case "LLC": obj.LLC(); break;
        case "Up": obj.Up(); break;
        case "Down": obj.Down(); break;
        default: break;
    }
}

//詳しくはキーコード表を見てくさい。
var KeyState = new Array();
document.onkeydown = function (event) {
    KeyState[event.keyCode] = true;
    if (event.keyCode != 122) {
        return false;
    }
}
document.onkeyup = function (event) {
    KeyState[event.keyCode] = false;
}
var deg;
function sensor_move(num) {

}
/* ウィンドウの移動とか */
var win = {
    x: [0, 0, 0],
    y: [0, 0, 0],
    mousestate: false,
    f: false,
    visible: true
}
function MD() {
    win.mousestate = true;
}
function MU() {
    win.mousestate = false;
    win.f = false;
}
document.onmousemove = function (e) {
    if (win.mousestate) {
        if (!win.f) {
            win.x[2] = e.clientX;
            win.y[2] = e.clientY;
            //windowの座標
            win.x[0] = win.x[1] = document.getElementById("window").offsetLeft;
            win.y[0] = win.y[1] = document.getElementById("window").offsetTop;
            win.f = true;
        }
        document.getElementById("window").style.left = parseInt(e.clientX - win.x[2] + win.x[0]) + "px";
        win.x[1] = parseInt(e.clientX - win.x[2] + win.x[0]);
        document.getElementById("window").style.top = parseInt(e.clientY - win.y[2] + win.y[0]) + "px";
        win.y[1] = parseInt(e.clientY - win.y[2] + win.y[0]);
    }
}
/********************************************/
//ウィンドウのやつを変えるやつ
function window_change(num) {
    if (num == 0) {
        //マイコンカーを消してコース作成を出す
        document.getElementById("cc_window").style.visibility = "visible";
        document.getElementById("ms_window").style.visibility = "hidden";
        document.getElementById("cc_button").style.backgroundColor = "rgba(199,209,225,1)";
        document.getElementById("ms_button").style.backgroundColor = "rgba(255,255,225,1)";
        arguments.callee.num = num;
    }
    else if (num == 1) {
        //コース作成を消してマイコンカーを出す
        document.getElementById("cc_window").style.visibility = "hidden";
        document.getElementById("ms_window").style.visibility = "visible";
        document.getElementById("cc_button").style.backgroundColor = "rgba(255,255,225,1)";
        document.getElementById("ms_button").style.backgroundColor = "rgba(199,209,225,1)";
        arguments.callee.num = num;
    }
    else if (num == 2) {
        //ウィンドウを非表示にする
        document.getElementById("window").style.visibility = "hidden";
        document.getElementById("cc_window").style.visibility = "hidden";
        document.getElementById("ms_window").style.visibility = "hidden";
    }
    else if (num == 3) {
        //ウィンドウを表示する
        document.getElementById("window").style.visibility = "visible";
        window_change(arguments.callee.num);
    }
}
